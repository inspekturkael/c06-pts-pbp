# C06 PTS PBP

Nama anggota:
- 2006596592	Mikael Alvian Rizky
- 2006596075	Muhammad Haqqi Al Farizi
- 2006596150	Mochammad Agus Yahya
- 2006595791	Dimas Ichsanul Arifin
- 2006596491	Charles Pramudana
- 2006596522	Gitan Sahl Tazakha Wijaya
- 2006595936	Jeremy Reeve Kurniawan


Link herokuapp: https://project-channel.herokuapp.com


Cerita Aplikasi
Pandemi ini telah berdampak pada seluruh kalangan masyarakat, termasuk programmer. Banyak yang kena PHK. Akibatnya, kita membuat web ini dengan tujuan agar programmer dapat mencari nafkah meskipun sudah kena PHK. Web ini berfungsi sebagai penyalur proyek dari orang yang ingin membuat proyek, tetapi tidak punya sumber daya manusia yang memadai kepada programmer yang tidak memiliki pekerjaan.


1. FAQ (form nambahin FAQ (ada di tampilan admin))  
    -  masing-masing bikin 1 pertanyaan
    - load daftar FAQ (ajax)
    <br>
2. Daftar Project (Searching Project, sekalian ajax) 

3. Profile (edit profile klien + programmer (ajax))

4. Melamar (Form melamar (object nyimpen ke array project, di load pake ajax))
    - Nama
    - Pesan
    - kontak yang dapat dihubungi
    - CV
    <br>
5. Halaman Project
    - Halaman project:
    - Nama project
    - kategori (frontend/backend)
    - bayaran
    - deskripsi
    - estimasi waktu
    - skill yang diminta
    - kontak klien
    - jumlah orang yang dibutuhkan
    - Daftar lamaran (ajax)
    <br>
6. Buat Project (Form data project (ajax kirim ke daftar project))
    - Nama project
    - kategori (frontend/backend)
    - bayaran
    - deskripsi
    - estimasi waktu
    - skill yang diminta
    - kontak klien
    - jumlah orang yang dibutuhkan
    <br>
7. Login/Logout (bikin buat user sama admin) + Sign up (Form data pengguna baru)
    - Login
    - email
    - password
    - Form sign up (ajax):
    - Nama
    - Jenis kelamin
    - Asal institusi
    - Email
    - Password
    - Kontak yang dapat dihubungi 
    <br>
Peran-peran pengguna:
- User biasa:
    -     Membuat proyek
    -     Melamar proyek
    -     Mengedit profile
    -     Melihat daftar pelamar pada proyek milikinya
    
- Admin:
    -     Menambah FAQ
    -     Melihat daftar pelamar pada semua proyek


from django.apps import AppConfig


class BuatProjectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buat_project'

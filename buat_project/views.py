from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .models import Project
from signup.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required

@login_required(login_url="/login/")
def buat_project(request):
    if request.method == 'POST':
        nama_pemilik = request.POST.get('nama_pemilik')
        nama_project = request.POST.get('nama_project')
        kategori = request.POST.get('kategori')
        bayaran = request.POST.get('bayaran')
        deskripsi = request.POST.get('deskripsi')
        estimasi = request.POST.get('estimasi')
        skills = request.POST.get('skills')
        kontak_pemilik = request.POST.get('kontak_pemilik')
        jumlah_orang = request.POST.get('jumlah_orang')

        print(nama_pemilik)
        print(request.POST)
        print(request.user.id)

        new_project = Project(
            nama_pemilik=nama_pemilik,
            nama_project=nama_project,
            kategori=kategori,
            bayaran=bayaran,
            deskripsi=deskripsi,
            estimasi=estimasi,
            skills=skills,
            kontak_pemilik=kontak_pemilik,
            jumlah_orang=jumlah_orang,
            id_pemilik=User.objects.get(id=request.user.id),
            )

        new_project.save()
        
        return JsonResponse({"instance": "Proyek Dihapus"}, status=200)

    return render(request, 'buat_project.html')
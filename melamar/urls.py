from django.urls import path
from melamar.views import add_lamar

urlpatterns = [
    path('lamar/<id>', add_lamar, name='add_lamar'),
]
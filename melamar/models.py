from django.db import models

# Create your models here.
class Melamar(models.Model):
    nama_pelamar = models.CharField(max_length=50, null=True)
    pesan = models.TextField(null=True)
    kontak_pelamar = models.TextField(null=True)
    cv_pelamar = models.FileField(null=True)
    id_project = models.IntegerField(default=0, null=True, blank=True)
    id_user = models.TextField(null=True, blank=True)
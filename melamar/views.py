from django.shortcuts import render
from django.http import HttpResponseRedirect
from melamar.models import Melamar
from melamar.forms import MelamarForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url="/login/")
def add_lamar(request, id):
    context ={}

    form = MelamarForm(request.POST or None, request.FILES or None)
    if(form.is_valid() and request.method == 'POST'):
        form = form.save(commit=False)
        id_project = int(id)
        form.id_project = id_project
        form.id_user = request.user.username
        form.save()
        return HttpResponseRedirect('../../../')
    context['form']= form
    return render(request, "melamar.html", context)
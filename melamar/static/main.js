$(document).on('submit', '#post-form',function(e){
    $.ajax({
        type:'POST',
        url:'{% url "add_lamar" %}',
        data:{
            nama_pelamar:$('#id_nama_pelamar').val(),
            pesan:$('#id_pesan').val(),
            kontak_pelamar:$('#id_kontak_pelamar').val(),
            cv_pelamar:$('#id_cv_pelamar').val(),
            id_project:$('#id_id_project').val(),
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },
        success:function(json){
            document.getElementById("post-form").reset();
            $(".posts").prepend('<div class="col-md-6">'+
                '<div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">' +
                    '<div class="col p-4 d-flex flex-column position-static">' +
                        '<h3 class="mb-0">' + json.nama_pelamar + '</h3>' +
                        '<p class="mb-auto">' + json.pesan + '</p>' +
                        '<p class="mb-auto">' + json.kontak_pelamar + '</p>' +
                        '<p class="mb-auto">' + json.cv_pelamar + '</p>' +
                        '<p class="mb-auto">' + json.id_project + '</p>' +
                    '</div>' +
                '</div>' +
            '</div>' 
            )
        },
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
    });
});
from django.urls import path
from .views import delete_project, index, edit_project, approve_project, download_cv, close_project

urlpatterns = [
    path('detail/<id>', index),
    path('detail/<id>/edit', edit_project),
    path('detail/closeproject/<id>', close_project),
    path('detail/deleteproject/<id>', delete_project),
    path('detail/approveproject/<id>', approve_project),
    path('detail/download/<filename>', download_cv),
]
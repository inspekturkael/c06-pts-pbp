from django.urls import path
from .views import index, create_q

app_name = 'faq'

urlpatterns = [
    path('', index, name='faq_index'),
    path('create_q/', create_q, name='create_q'),
]

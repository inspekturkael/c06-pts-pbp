from django import forms
from .models import Faq

class FaqForm(forms.ModelForm):
    class Meta:
        model = Faq
        fields = ['question']

        labels = {
            'question' : ''
        }

        widgets = {
            'question' : forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Pertanyaan', 'id':'q_id'}),
        }
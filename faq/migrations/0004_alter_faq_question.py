# Generated by Django 3.2.7 on 2021-10-29 06:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('faq', '0003_remove_faq_is_answered'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faq',
            name='question',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]

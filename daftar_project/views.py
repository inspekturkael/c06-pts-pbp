from django.http import response
from .forms import SuscribeForm
from buat_project.models import Project
from django.core import serializers
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

def index(request):
    form = SuscribeForm(request.POST or None)
    template = ""
    context = {
		'title':'Daftar Proyek',
		'heading':'Daftar Proyek',
        'subscribeForm':form,
    }

    if (form.is_valid() and request.method == 'POST'):
        request.user.is_subscribed = True
        request.user.save()
        return redirect('/')

    if(request.user.is_authenticated):
        if (request.user.is_subscribed):
            template = "daftar_project/index/index_daftar_project.html"
        else:
            template = "daftar_project/index/index_daftar_project_member_nonsubscribed.html"
    else:
        template = "daftar_project/index/index_daftar_project_nonmember.html"
    return render(request, template, context)

def get_project(request):
    project = Project.objects.all()
    data = serializers.serialize('json', project)
    if(request.user.is_authenticated):
        user_id = request.user.id
        is_admin = request.user.is_admin
        response = {
            'projects':data,
            'user_id':user_id,
            'is_admin':is_admin
            }
    else:
        response = {
            'projects':data,
            }
    return JsonResponse(response)

def my_project(request):
    context = {
		'title':'Proyek Saya',
		'heading':'Proyek Saya',
    }

    if(request.user.is_authenticated):
        return render(request, 'daftar_project/my_project/my_project.html', context)
    else:
        return redirect('/login')
    
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from login.forms import UserAuthenticationForm
from django.http.response import Http404, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import AnonymousUser

# Create your views here.
def login_view(request):
	context = {}
	user = request.user
	if user.is_authenticated:
		return redirect('daftar_project:index_daftar_project')

	if request.POST:
		form = UserAuthenticationForm(request.POST)
		if form.is_valid():
			email = request.POST['email']
			password = request.POST['password']
			user = authenticate(email=email, password=password)
			if user:
				login(request, user)
				return redirect('daftar_project:index_daftar_project')
				
	else:
		form = UserAuthenticationForm()
	context['login_form'] = form

	return render(request, "login.html", context)

from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from .views import login_view

app_name = 'login'

urlpatterns = [
    url('', login_view, name='login'),
]
from django.shortcuts import render, redirect
from django.http.response import HttpResponseRedirect, JsonResponse
from django.http.response import HttpResponse
from signup.models import User
from .forms import UpdateProfil
from django.http import JsonResponse
#from .form import classForm
# Create your views here.
def index(request):
    if not request.user.is_authenticated:
        return redirect("/login") 
    users = User.objects.all().values()
    response = {'users':users}
    return render(request, 'PageProfil.html', response)
def edit(request, *args, **kwargs):
    if not request.user.is_authenticated:
        return redirect("/login") 
    user_id = request.user.id
    try:
        account = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return HttpResponse("Something went wrong")
    context = {}
    if request.POST:
        form = UpdateProfil(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/buat_profile')
        else :
            form = UpdateProfil(request.POST, instance= request.user,       
                initial = {
                    "nama" : account.nama,
                    "jenis_kelamin" : account.jenis_kelamin,
                    "institusi" : account.institusi,
                    "email" : account.email,
                    "kontak" : account.kontak
                }   
            )
            context['form'] = form
            validate_email(request)
    else :
        form = UpdateProfil(request.POST, instance= request.user,       
                initial = {
                    "nama" : account.nama,
                    "jenis_kelamin" : account.jenis_kelamin,
                    "institusi" : account.institusi,
                    "email" : account.email,
                    "kontak" : account.kontak
                }   
            ) 
        context['form'] = form
    validate_email(request)
    return render(request, 'FormProfil.html', context)

def validate_email(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken': User.objects.exclude(id = request.user.id).filter(email__iexact=email).exists()
    }
    return JsonResponse(data)
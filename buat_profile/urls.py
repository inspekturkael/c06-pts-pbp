from django.urls import path
from .views import index, edit, validate_email
from django.conf.urls import url

app_name = 'profile'

urlpatterns = [
    path('', index, name='index'),
    path('edit', edit),
    url('validate_email/', validate_email, name='validate_email'),
]
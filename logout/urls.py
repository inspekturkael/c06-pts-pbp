from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from .views import logout_view

app_name = 'logout'

urlpatterns = [
    url('', logout_view, name='logout')
]